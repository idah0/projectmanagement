﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using JJ.ProjectManagement.EntityFramework;

namespace JJ.ProjectManagement
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(ProjectManagementCoreModule))]
    public class ProjectManagementDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ProjectManagementDbContext>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
