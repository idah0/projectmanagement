﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JJ.ProjectManagement.EntityFramework;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Projects;
using JJ.ProjectManagement.Sprints;

namespace JJ.ProjectManagement.Migrations.SeedData
{
    public class DefaultProjectAndSprintCreator
    {
        private readonly ProjectManagementDbContext _context;

        public DefaultProjectAndSprintCreator(ProjectManagementDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            var project = new Project
            {
                Name = "Project One",
            };
            var sprint = new Sprint
            {
                Name = "Sprint 1",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(7)
            };
            var issues = new List<Issue>()
            {
                new Issue
                {
                    Name = "test1",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7)
                },
                new Issue
                {
                    Name = "test2",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7)
                },
                new Issue
                {
                    Name = "test3",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7)
                },
                new Issue
                {
                    Name = "test4",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7)
                },
                new Issue
                {
                    Name = "test5",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7)
                },
                new Issue
                {
                    Name = "test6",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7)
                },
                new Issue
                {
                    Name = "test7",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7)
                },
                new Issue
                {
                    Name = "test8",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7)
                }
            };
            sprint.Issues = issues;

            project.Sprints = new List<Sprint>() { sprint };

            _context.Projects.Add(project);
        }
    }
}
