﻿using JJ.ProjectManagement.EntityFramework;
using EntityFramework.DynamicFilters;

namespace JJ.ProjectManagement.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly ProjectManagementDbContext _context;

        public InitialHostDbBuilder(ProjectManagementDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
