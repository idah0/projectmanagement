﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JJ.ProjectManagement.EntityFramework;
using JJ.ProjectManagement.Issues;

namespace JJ.ProjectManagement.Migrations.SeedData
{
    public class DefaultIssueCategoryCreator
    {
        private readonly ProjectManagementDbContext _context;

        public DefaultIssueCategoryCreator(ProjectManagementDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            if (!_context.IssueCategories.Any())
            {
                var catList = new List<IssueCategory>()
                {
                    new IssueCategory {Name = "User Story"},
                    new IssueCategory {Name = "Feature"},
                    new IssueCategory {Name = "Design"},
                    new IssueCategory {Name = "Bug"},
                    new IssueCategory {Name = "Research"},
                };
                catList.ForEach(x =>
                {
                    _context.IssueCategories.Add(x);
                    _context.SaveChanges();
                });
            }
        }
    }
}
