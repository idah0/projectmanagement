﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JJ.ProjectManagement.EntityFramework;
using JJ.ProjectManagement.Members;

namespace JJ.ProjectManagement.Migrations.SeedData
{
    public class DefaultMemberCategoryCreator
    {
        private readonly ProjectManagementDbContext _context;

        public DefaultMemberCategoryCreator(ProjectManagementDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            if (!_context.MembersCategories.Any())
            {
                var catList = new List<MembersCategory>()
                {
                    new MembersCategory {Name = "Manager"},
                    new MembersCategory {Name = "Developer"},
                    new MembersCategory {Name = "Tester"},
                    new MembersCategory {Name = "Designer"},
                    new MembersCategory {Name = "Tech Lead"},
                };
                catList.ForEach(x =>
                {
                    _context.MembersCategories.Add(x);
                    _context.SaveChanges();
                });
            }
        }
    }
}
