namespace JJ.ProjectManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_base_models : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IssueCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Issues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        Done = c.Int(nullable: false),
                        UserForeignKey = c.Long(),
                        EstimatedTime = c.Double(nullable: false),
                        IssueCategoryForeignKey = c.Int(),
                        SprintForeignKey = c.Long(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.UserForeignKey)
                .ForeignKey("dbo.IssueCategories", t => t.IssueCategoryForeignKey)
                .ForeignKey("dbo.Sprints", t => t.SprintForeignKey)
                .Index(t => t.UserForeignKey)
                .Index(t => t.IssueCategoryForeignKey)
                .Index(t => t.SprintForeignKey);
            
            CreateTable(
                "dbo.LoggedTimes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LogTime = c.Double(nullable: false),
                        LogDate = c.DateTime(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        User_Id = c.Long(),
                        Issue_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.User_Id)
                .ForeignKey("dbo.Issues", t => t.Issue_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Issue_Id);
            
            CreateTable(
                "dbo.Sprints",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        ProjectForeignKey = c.Long(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.ProjectForeignKey)
                .Index(t => t.ProjectForeignKey);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Members",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberCategoryForeignKey = c.Int(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        User_Id = c.Long(),
                        Project_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MembersCategories", t => t.MemberCategoryForeignKey)
                .ForeignKey("dbo.AbpUsers", t => t.User_Id)
                .ForeignKey("dbo.Projects", t => t.Project_Id)
                .Index(t => t.MemberCategoryForeignKey)
                .Index(t => t.User_Id)
                .Index(t => t.Project_Id);
            
            CreateTable(
                "dbo.MembersCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sprints", "ProjectForeignKey", "dbo.Projects");
            DropForeignKey("dbo.Members", "Project_Id", "dbo.Projects");
            DropForeignKey("dbo.Members", "User_Id", "dbo.AbpUsers");
            DropForeignKey("dbo.Members", "MemberCategoryForeignKey", "dbo.MembersCategories");
            DropForeignKey("dbo.Issues", "SprintForeignKey", "dbo.Sprints");
            DropForeignKey("dbo.LoggedTimes", "Issue_Id", "dbo.Issues");
            DropForeignKey("dbo.LoggedTimes", "User_Id", "dbo.AbpUsers");
            DropForeignKey("dbo.Issues", "IssueCategoryForeignKey", "dbo.IssueCategories");
            DropForeignKey("dbo.Issues", "UserForeignKey", "dbo.AbpUsers");
            DropIndex("dbo.Members", new[] { "Project_Id" });
            DropIndex("dbo.Members", new[] { "User_Id" });
            DropIndex("dbo.Members", new[] { "MemberCategoryForeignKey" });
            DropIndex("dbo.Sprints", new[] { "ProjectForeignKey" });
            DropIndex("dbo.LoggedTimes", new[] { "Issue_Id" });
            DropIndex("dbo.LoggedTimes", new[] { "User_Id" });
            DropIndex("dbo.Issues", new[] { "SprintForeignKey" });
            DropIndex("dbo.Issues", new[] { "IssueCategoryForeignKey" });
            DropIndex("dbo.Issues", new[] { "UserForeignKey" });
            DropTable("dbo.MembersCategories");
            DropTable("dbo.Members");
            DropTable("dbo.Projects");
            DropTable("dbo.Sprints");
            DropTable("dbo.LoggedTimes");
            DropTable("dbo.Issues");
            DropTable("dbo.IssueCategories");
        }
    }
}
