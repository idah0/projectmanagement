using System.Data.Entity.Migrations;
using Abp.MultiTenancy;
using Abp.Zero.EntityFramework;
using JJ.ProjectManagement.Migrations.SeedData;
using EntityFramework.DynamicFilters;

namespace JJ.ProjectManagement.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<ProjectManagement.EntityFramework.ProjectManagementDbContext>, IMultiTenantSeed
    {
        public AbpTenantBase Tenant { get; set; }

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ProjectManagement";
        }

        protected override void Seed(ProjectManagement.EntityFramework.ProjectManagementDbContext context)
        {
            context.DisableAllFilters();

            if (Tenant == null)
            {
                //Host seed
                new InitialHostDbBuilder(context).Create();

                //Default tenant seed (in host database).
                new DefaultTenantCreator(context).Create();
                new TenantRoleAndUserBuilder(context, 1).Create();

                new DefaultMemberCategoryCreator(context).Create();
                new DefaultIssueCategoryCreator(context).Create();
                new DefaultProjectAndSprintCreator(context).Create();
            }
            else
            {
                //You can add seed for tenant databases and use Tenant property...
            }

            context.SaveChanges();
        }
    }
}
