namespace JJ.ProjectManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_base_models_fix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Issues", "Issue_Id", c => c.Long());
            CreateIndex("dbo.Issues", "Issue_Id");
            AddForeignKey("dbo.Issues", "Issue_Id", "dbo.Issues", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Issues", "Issue_Id", "dbo.Issues");
            DropIndex("dbo.Issues", new[] { "Issue_Id" });
            DropColumn("dbo.Issues", "Issue_Id");
        }
    }
}
