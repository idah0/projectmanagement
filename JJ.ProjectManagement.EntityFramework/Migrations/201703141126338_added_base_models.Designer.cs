// <auto-generated />
namespace JJ.ProjectManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class added_base_models : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(added_base_models));
        
        string IMigrationMetadata.Id
        {
            get { return "201703141126338_added_base_models"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
