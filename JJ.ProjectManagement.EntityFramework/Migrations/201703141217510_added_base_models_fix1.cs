namespace JJ.ProjectManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_base_models_fix1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Members");
            AlterColumn("dbo.Members", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.Members", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Members");
            AlterColumn("dbo.Members", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Members", "Id");
        }
    }
}
