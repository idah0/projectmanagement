namespace JJ.ProjectManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_projectID_to_Members : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Members", name: "Project_Id", newName: "ProjectId");
            RenameIndex(table: "dbo.Members", name: "IX_Project_Id", newName: "IX_ProjectId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Members", name: "IX_ProjectId", newName: "IX_Project_Id");
            RenameColumn(table: "dbo.Members", name: "ProjectId", newName: "Project_Id");
        }
    }
}
