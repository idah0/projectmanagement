﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace JJ.ProjectManagement.EntityFramework.Repositories
{
    public abstract class ProjectManagementRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<ProjectManagementDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected ProjectManagementRepositoryBase(IDbContextProvider<ProjectManagementDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class ProjectManagementRepositoryBase<TEntity> : ProjectManagementRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected ProjectManagementRepositoryBase(IDbContextProvider<ProjectManagementDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
