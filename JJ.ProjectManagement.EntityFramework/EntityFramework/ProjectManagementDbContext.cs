﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using JJ.ProjectManagement.Authorization.Roles;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Members;
using JJ.ProjectManagement.MultiTenancy;
using JJ.ProjectManagement.Projects;
using JJ.ProjectManagement.Sprints;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.EntityFramework
{
    public class ProjectManagementDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        public virtual IDbSet<MembersCategory> MembersCategories { get; set; }
        public virtual IDbSet<IssueCategory> IssueCategories { get; set; }
        public virtual IDbSet<LoggedTime> LoggedTimes { get; set; }

        public virtual IDbSet<Member> Members { get; set; }
        public virtual IDbSet<Issue> Issues { get; set; }
        public virtual IDbSet<Sprint> Sprints { get; set; }
        public virtual IDbSet<Project> Projects { get; set; }


        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public ProjectManagementDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in ProjectManagementDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of ProjectManagementDbContext since ABP automatically handles it.
         */
        public ProjectManagementDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public ProjectManagementDbContext(DbConnection connection)
            : base(connection, true)
        {

        }
    }
}
