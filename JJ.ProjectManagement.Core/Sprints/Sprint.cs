﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Projects;

namespace JJ.ProjectManagement.Sprints
{
    public class Sprint : AuditedEntity<long>
    {
        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public virtual ICollection<Issue> Issues { get; set; }

        [ForeignKey("ProjectForeignKey")]
        public Project Project { get; set; }
        public virtual long? ProjectForeignKey { get; set; }
    }
}
