﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;

namespace JJ.ProjectManagement.Members
{
    public class MembersCategory : Entity
    {
        public string Name { get; set; }
    }
}
