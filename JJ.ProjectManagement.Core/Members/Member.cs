﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using JJ.ProjectManagement.Projects;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Members
{
    public class Member : AuditedEntity<long>
    {
        public User User { get; set; }

        [ForeignKey("MemberCategoryForeignKey")]
        public MembersCategory MemberCategory { get; set; }
        public virtual int? MemberCategoryForeignKey { get; set; }

        [ForeignKey("ProjectId")]  
        public virtual Project Project { get; set; }
        public virtual long? ProjectId { get; set; }
    }
}
