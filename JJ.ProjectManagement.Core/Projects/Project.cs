﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using JJ.ProjectManagement.Members;
using JJ.ProjectManagement.Sprints;

namespace JJ.ProjectManagement.Projects
{
    public class Project : AuditedEntity<long>
    {
        public string Name { get; set; }

        public virtual ICollection<Member> Members { get; set; }

        public virtual ICollection<Sprint> Sprints { get; set; }
    }
}
