﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJ.ProjectManagement.Idinput
{
    public class IdInput<T>
    {
        public T Id { get; set; }
    }
}
