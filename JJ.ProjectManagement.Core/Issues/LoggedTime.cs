﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Issues
{
    public class LoggedTime : AuditedEntity
    {
        public User User { get; set; }

        public double LogTime { get; set; }

        public DateTime LogDate { get;set; }
    }
}
