﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using JJ.ProjectManagement.Sprints;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Issues
{
    public class Issue : AuditedEntity<long>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual ICollection<Issue> SubIssues { get; set; }

        public int Done { get; set; }

        [ForeignKey("UserForeignKey")]
        public User AssignedUser { get; set; }
        public virtual long? UserForeignKey { get; set; }

        public double EstimatedTime { get; set; }

        public virtual ICollection<LoggedTime> LoggedTime { get; set; }

        [ForeignKey("IssueCategoryForeignKey")]
        public IssueCategory IssueCategory { get; set; }
        public virtual int? IssueCategoryForeignKey { get; set; }

        [ForeignKey("SprintForeignKey")]
        public Sprint Sprint { get; set; }
        public virtual long? SprintForeignKey { get; set; }

    }
}
