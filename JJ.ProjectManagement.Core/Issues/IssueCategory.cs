﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;

namespace JJ.ProjectManagement.Issues
{
    public class IssueCategory : Entity
    {
        public string Name { get; set; }
    }
}
