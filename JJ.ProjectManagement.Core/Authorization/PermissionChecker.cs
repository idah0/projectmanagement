﻿using Abp.Authorization;
using JJ.ProjectManagement.Authorization.Roles;
using JJ.ProjectManagement.MultiTenancy;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Authorization
{
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
