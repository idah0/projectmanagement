﻿using Abp.MultiTenancy;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}