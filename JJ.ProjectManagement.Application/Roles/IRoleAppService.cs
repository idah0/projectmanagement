﻿using System.Threading.Tasks;
using Abp.Application.Services;
using JJ.ProjectManagement.Roles.Dto;

namespace JJ.ProjectManagement.Roles
{
    public interface IRoleAppService : IApplicationService
    {
        Task UpdateRolePermissions(UpdateRolePermissionsInput input);
    }
}
