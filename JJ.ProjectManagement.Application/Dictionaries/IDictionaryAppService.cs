﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using JJ.ProjectManagement.Dictionaries.dto;
using JJ.ProjectManagement.Idinput;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Members;

namespace JJ.ProjectManagement.Dictionaries
{
    public interface IDictionaryAppService : IApplicationService
    {
        List<IssueCategory> GetIssueCategories();

        List<MemberDto> GetAssignedUsers(IdInput<long> input);

        List<MembersCategory> GetMembersCategories();

        List<UserDto> GetAllUsers();
    }

}
