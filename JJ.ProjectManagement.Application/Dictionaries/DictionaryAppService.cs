﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using JJ.ProjectManagement.Dictionaries.dto;
using JJ.ProjectManagement.Idinput;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Members;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Dictionaries
{
    public class DictionaryAppService : IDictionaryAppService
    {
        private readonly IRepository<IssueCategory> _issueCategoryRepository;
        private readonly IRepository<MembersCategory> _membersCategoryRepository;
        private readonly IRepository<Member, long> _membersRepository;
        private readonly IRepository<User, long> _userRepository;

        public DictionaryAppService(IRepository<IssueCategory> issueCategoryRepository, IRepository<Member, long> membersRepository, IRepository<User, long> userRepository, IRepository<MembersCategory> membersCategoryRepository)
        {
            _issueCategoryRepository = issueCategoryRepository;
            _membersRepository = membersRepository;
            _userRepository = userRepository;
            _membersCategoryRepository = membersCategoryRepository;
        }


        public List<IssueCategory> GetIssueCategories()
        {
            return _issueCategoryRepository.GetAll().ToList();
        }

        public List<MemberDto> GetAssignedUsers(IdInput<long> input)
        {
            var result = _membersRepository.GetAllIncluding(x => x.User,x=>x.MemberCategory).Where(x=>x.ProjectId == input.Id).ToList();
            return result.MapTo<List<MemberDto>>();
        }

        public List<UserDto> GetAllUsers()
        {
            var users = _userRepository.GetAll().ToList();
            return users.MapTo<List<UserDto>>();
        }

        public List<MembersCategory> GetMembersCategories()
        {
            var categories = _membersCategoryRepository.GetAll().ToList();
            return categories;
        }
    }
}
