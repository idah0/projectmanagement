﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Members;

namespace JJ.ProjectManagement.Dictionaries.dto
{
    [AutoMap(typeof(Member))]
    public class MemberDto
    {
        public UserDto User { get; set; }

        public MembersCategory MemberCategory { get; set; }

    }
}
