﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Dictionaries.dto
{
    [AutoMap(typeof(User))]
    public class UserDto
    {
        public long Id { get; set; }
        public string FullName { get; set; }
    }
}
