﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Sprints;

namespace JJ.ProjectManagement.Projects.Dto
{
    [AutoMapFrom(typeof(Sprint))]
    public class GetSprintDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public long NrOfIssues { get; set; }

    }
}
