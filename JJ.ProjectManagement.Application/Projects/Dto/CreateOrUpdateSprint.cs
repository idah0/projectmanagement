﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Sprints;

namespace JJ.ProjectManagement.Projects.Dto
{
    [AutoMapTo(typeof(Sprint))]
    public class CreateOrUpdateSprint
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public long ProjectId { get; set; }

    }
}
