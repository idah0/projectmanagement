﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJ.ProjectManagement.Projects.Dto
{
    public class MembersListDto
    {
        public ICollection<AddMembersDto> Members { get; set; }

        public long ProjectId { get; set; }
    }
}
