﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace JJ.ProjectManagement.Projects.Dto
{
    [AutoMapFrom(typeof(Project))]
    public class GetProjectDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
