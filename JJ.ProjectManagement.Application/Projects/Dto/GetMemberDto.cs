﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Members;

namespace JJ.ProjectManagement.Projects.Dto
{
    [AutoMapFrom(typeof(Member))]
    public class GetMemberDto
    {
        public GetUserDto User { get; set; }
        public int MemberCategoryForeignKey { get; set; }
    }
}
