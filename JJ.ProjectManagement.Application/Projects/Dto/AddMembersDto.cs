﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Members;

namespace JJ.ProjectManagement.Projects.Dto
{
    [AutoMapTo(typeof(Member))]
    public class AddMembersDto
    {
        public long UserId { get; set; }

        public int MembersCategoryId { get; set; }
    }
}
