﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Projects.Dto
{
    [AutoMapFrom(typeof(User))]
    public class GetUserDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
