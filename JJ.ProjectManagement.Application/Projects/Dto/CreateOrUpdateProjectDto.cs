﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace JJ.ProjectManagement.Projects.Dto
{
    [AutoMapTo(typeof(Project))]
    public class CreateOrUpdateProjectDto
    {
        public long? Id { get; set; }

        public string Name { get; set; }
    }
}
