﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Castle.Core.Internal;
using JJ.ProjectManagement.Idinput;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Members;
using JJ.ProjectManagement.Projects.Dto;
using JJ.ProjectManagement.Sprints;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Projects
{
    class ProjectsAppService : IProjectsAppService
    {
        private readonly IRepository<Project, long> _projectsRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<MembersCategory> _membersCategoryRepository;
        private readonly IRepository<Sprint, long> _sprintRepository;
        private readonly IRepository<Member, long> _membersRepository;
        private readonly IRepository<Issue, long> _issueRepository;
        private readonly IRepository<LoggedTime> _loggedTimeRepository;

        public ProjectsAppService(IRepository<Project, long> projectsRepository, IRepository<User, long> userRepository, IRepository<MembersCategory> membersCategoryRepository, IRepository<Sprint, long> sprintRepository, IRepository<Member, long> membersRepository, IRepository<Issue, long> issueRepository, IRepository<LoggedTime> loggedTimeRepository)
        {
            _projectsRepository = projectsRepository;
            _userRepository = userRepository;
            _membersCategoryRepository = membersCategoryRepository;
            _sprintRepository = sprintRepository;
            _membersRepository = membersRepository;
            _issueRepository = issueRepository;
            _loggedTimeRepository = loggedTimeRepository;
        }


        public async Task<long> CreateOrUpdateProject(CreateOrUpdateProjectDto input)
        {
            if (input.Id.HasValue)
            {
                return await UpdateProject(input);
            }
            else
            {
                return await CreateProject(input);
            }
        }

        public async Task DeleteProject(IdInput<long> input)
        {
            var project =
                _projectsRepository.GetAllIncluding(x => x.Sprints, c => c.Members)
                    .FirstOrDefault(x => x.Id == input.Id);
            //TODO:Implement Checks
            await _projectsRepository.DeleteAsync(project);
        }

        public async Task<IEnumerable<GetProjectDto>> GetAllProjects()
        {
            var projects = _projectsRepository.GetAll();
            var result = projects.MapTo<List<GetProjectDto>>();
            return result;
        }

        public async Task<GetSingleProjectDto> GetSingleProject(IdInput<long> input)
        {
            var project = _projectsRepository.GetAllIncluding(x => x.Members, x => x.Sprints, x => x.Sprints.Select(z => z.Issues), x=>x.Members.Select(z=>z.User)).FirstOrDefault(x => x.Id == input.Id);
            var result = project.MapTo<GetSingleProjectDto>();
            foreach (var x in project.Sprints)
            {
                result.Sprints.FirstOrDefault(v => v.Id == x.Id).NrOfIssues = x.Issues.Count;
            }
            return result;
        }

        private async Task<long> CreateProject(CreateOrUpdateProjectDto input)
        {
            var project = input.MapTo<Project>();
            return await _projectsRepository.InsertAndGetIdAsync(project);
        }
        private async Task<long> UpdateProject(CreateOrUpdateProjectDto input)
        {
            var project = _projectsRepository.Get(input.Id.Value);
            project.Name = input.Name;
            return await _projectsRepository.InsertOrUpdateAndGetIdAsync(project);
        }

        public IEnumerable<GetUserDto> GetAllUsers()
        {
            var users = _userRepository.GetAll();
            var result = users.MapTo<List<GetUserDto>>();
            return result;
        }

        public async Task AddMembersToProject(MembersListDto input)
        {
            var project =
                _projectsRepository.GetAllIncluding(x => x.Sprints, c => c.Members)
                    .FirstOrDefault(x => x.Id == input.ProjectId);
            foreach (var x in project.Members.ToArray())
            {
                _membersRepository.Delete(x);
            }
            var MembersList = new List<Member>();
            foreach (var x in input.Members)
            {
                var member = new Member
                {
                    User = _userRepository.FirstOrDefault(z => z.Id == x.UserId),
                    MemberCategory = _membersCategoryRepository.FirstOrDefault(z => z.Id == x.MembersCategoryId),
                    MemberCategoryForeignKey = x.MembersCategoryId
                };
                MembersList.Add(member);
            }
            project.Members = MembersList;
            await _projectsRepository.UpdateAsync(project);
        }

        public async Task<long> CreateOrUpdateSprint(CreateOrUpdateSprint input)
        {
            if (input.Id.HasValue)
            {
                return await UpdateSprint(input);
            }
            else
            {
                return await CreateSprint(input);
            }
        }

        private async Task<long> CreateSprint(CreateOrUpdateSprint input)
        {
            var project =
                _projectsRepository.GetAllIncluding(x => x.Sprints).FirstOrDefault(x => x.Id == input.ProjectId);
            var sprint = input.MapTo<Sprint>();
            if (project.Sprints == null)
            {
                project.Sprints = new List<Sprint>();
            }
            project.Sprints.Add(sprint);
            return await _projectsRepository.InsertOrUpdateAndGetIdAsync(project);

        }
        private async Task<long> UpdateSprint(CreateOrUpdateSprint input)
        {
            var sprintToUpdate = _sprintRepository.FirstOrDefault(x => x.Id == input.Id);
            sprintToUpdate.Name = input.Name;
            return await _sprintRepository.InsertOrUpdateAndGetIdAsync(sprintToUpdate);
        }

        public async Task DeleteSprint(IdInput<long> input)
        {
            var sprint = _sprintRepository.GetAllIncluding(x=>x.Issues, x=>x.Issues.Select(t=>t.LoggedTime)).FirstOrDefault(x => x.Id == input.Id);
            foreach (var x in sprint.Issues.ToArray())
            {
                foreach (var y in x.LoggedTime.ToArray())
                {
                    await _loggedTimeRepository.DeleteAsync(y.Id);
                }
                await _issueRepository.DeleteAsync(x.Id);
            }
            await _sprintRepository.DeleteAsync(sprint.Id);
        }
    }
}
