﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using JJ.ProjectManagement.Idinput;
using JJ.ProjectManagement.Projects.Dto;

namespace JJ.ProjectManagement.Projects
{
    public interface IProjectsAppService : IApplicationService
    {
        Task<long> CreateOrUpdateProject(CreateOrUpdateProjectDto input);

        Task DeleteProject(IdInput<long> input);

        Task DeleteSprint(IdInput<long> input);

        Task<IEnumerable<GetProjectDto>> GetAllProjects();

        Task<GetSingleProjectDto> GetSingleProject(IdInput<long> input);

        IEnumerable<GetUserDto> GetAllUsers();

        Task AddMembersToProject(MembersListDto input);

        Task<long> CreateOrUpdateSprint(CreateOrUpdateSprint input);
    }

}
