﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Users;
using JJ.ProjectManagement.WorkTime.Dto;

namespace JJ.ProjectManagement.WorkTime
{
    public class WorkTimeAppService : ProjectManagementAppServiceBase,IWorkTimeAppService
    {
        private IRepository<Issue, long> _issueRepository;
        private IRepository<User, long> _userRepository;

        public WorkTimeAppService(IRepository<Issue, long> issueRepository, IRepository<User, long> userRepository)
        {
            _issueRepository = issueRepository;
            _userRepository = userRepository;
        }


        public List<GetWorkTimeDto> GetWorkTime(GetWorkTimeParams input)
        {
            var startDate = new DateTime(input.Year, input.Month, 1);
            var endDate = startDate.AddMonths(1);

            var issues = _issueRepository.GetAllIncluding(x => x.LoggedTime, x=>x.LoggedTime.Select(t=>t.User)).Where(x => x.StartDate.Month == startDate.Month && x.StartDate.Year == startDate.Year).ToList();
            var result = new List<GetWorkTimeDto>();
            foreach (var x in issues)
            {
                foreach (var y in x.LoggedTime)
                {
                    if (y.User.Id == AbpSession.UserId.Value)
                    {
                        result.Add(new GetWorkTimeDto
                        {
                            LoggedTime = y.LogTime,
                            DateLogged = y.LogDate,
                            IssueName = x.Name
                        });
                    }
                }
            }
            return result;
        }
    }
}
