﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJ.ProjectManagement.WorkTime.Dto
{
    public class GetWorkTimeDto
    {
        public string IssueName { get; set; }
        public double LoggedTime { get; set; }
        public DateTime DateLogged { get; set; }
    }
}
