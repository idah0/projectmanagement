﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJ.ProjectManagement.WorkTime.Dto
{
    public class GetWorkTimeParams
    {
        public int Year;
        public int Month;
    }
}
