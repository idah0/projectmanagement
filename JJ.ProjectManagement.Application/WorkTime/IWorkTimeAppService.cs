﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using JJ.ProjectManagement.WorkTime.Dto;

namespace JJ.ProjectManagement.WorkTime
{
    public interface IWorkTimeAppService : IApplicationService
    {
        List<GetWorkTimeDto> GetWorkTime(GetWorkTimeParams input);
    }
}
