﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace JJ.ProjectManagement.Sprints.Dto
{
    
    public class GetIssuesForSprintDto
    {
        public int NrOfPages { get; set; }

        public IEnumerable<GetIssueInfoDto> Issues { get; set; }
    }
}
