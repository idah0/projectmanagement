﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Dictionaries.dto;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Projects.Dto;
using JJ.ProjectManagement.Users;

namespace JJ.ProjectManagement.Sprints.Dto
{
    [AutoMapFrom(typeof(Issue))]
    public class GetIssueInfoDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int Done { get; set; }

        public UserDto AssignedUser { get; set; }

        public double EstimatedTime { get; set; }

        public IssueCategory IssueCategory { get; set; }
    }
}
