﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJ.ProjectManagement.Sprints.Dto
{
    public class GetPagedIssuesDto
    {
        public long Id { get; set; }

        public int ItemsPerPage { get; set; }

        public int PageNumber { get; set; }
    }
}
