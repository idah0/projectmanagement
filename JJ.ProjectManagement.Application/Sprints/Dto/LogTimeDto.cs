﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Issues;

namespace JJ.ProjectManagement.Sprints.Dto
{
    [AutoMap(typeof(LoggedTime))]
    public class LogTimeDto
    {
        public long IssueId { get; set; }
        public double LogTime { get; set; }

        public DateTime LogDate { get; set; }
    }
}
