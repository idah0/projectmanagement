﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace JJ.ProjectManagement.Sprints.Dto
{
    [AutoMapFrom(typeof(Sprint))]
    class GetSprintDto
    {
        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public long NrOfIssues { get; set; }
    }
}
