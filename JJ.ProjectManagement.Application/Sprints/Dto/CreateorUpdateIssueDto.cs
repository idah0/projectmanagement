﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JJ.ProjectManagement.Issues;

namespace JJ.ProjectManagement.Sprints.Dto
{
    [AutoMap(typeof(Issue))]
    public class CreateorUpdateIssueDto
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual ICollection<Issue> SubIssues { get; set; }

        public int Done { get; set; }

        public long UserForeignKey { get; set; }

        public double EstimatedTime { get; set; }

        public ICollection<LoggedTime> LoggedTime { get; set; }

        public  int IssueCategoryForeignKey { get; set; }

        public virtual long SprintForeignKey { get; set; }

        public long? ParentIssueId { get; set; }
    }
}
