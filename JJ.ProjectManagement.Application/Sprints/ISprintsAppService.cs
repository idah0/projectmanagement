﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using JJ.ProjectManagement.Idinput;
using JJ.ProjectManagement.Sprints.Dto;
using GetSprintDto = JJ.ProjectManagement.Projects.Dto.GetSprintDto;

namespace JJ.ProjectManagement.Sprints
{
    public interface ISprintsAppService : IApplicationService
    {
        GetSprintDto GetSprintInfo(IdInput<long> input);

        GetIssuesForSprintDto GetIssuesForSprint(GetPagedIssuesDto input);

        CreateorUpdateIssueDto GetIssueForEdit(IdInput<long> input);

        Task DeleteIssue(IdInput<long> input);

        Task LogTime(LogTimeDto input);

        Task<GetSprintDto> GetSprintForEdit(IdInput<long> input);

        Task<long> CreateOrUpdateIssue(CreateorUpdateIssueDto input);
    }
}
