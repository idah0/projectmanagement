﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using JJ.ProjectManagement.Idinput;
using JJ.ProjectManagement.Issues;
using JJ.ProjectManagement.Sprints.Dto;
using JJ.ProjectManagement.Users;
using GetSprintDto = JJ.ProjectManagement.Projects.Dto.GetSprintDto;

namespace JJ.ProjectManagement.Sprints
{
    public class SprintsAppService : ProjectManagementAppServiceBase, ISprintsAppService
    {
        private readonly IRepository<Sprint, long> _sprintRepository;
        private readonly IRepository<Issue, long> _issueRepository;
        private readonly IRepository<IssueCategory> _issueCategoryRepository;
        private readonly IRepository<User, long> _userRepository;

        public SprintsAppService(IRepository<Sprint, long> sprintRepository, IRepository<Issue, long> issueRepository, IRepository<IssueCategory> issueCategoryRepository, IRepository<User, long> userRepository)
        {
            _sprintRepository = sprintRepository;
            _issueRepository = issueRepository;
            _issueCategoryRepository = issueCategoryRepository;
            _userRepository = userRepository;
        }

        public GetSprintDto GetSprintInfo(IdInput<long> input)
        {
            var sprint = _sprintRepository.GetAllIncluding(x => x.Issues).FirstOrDefault(x => x.Id == input.Id);
            var result = sprint.MapTo<GetSprintDto>();
            result.NrOfIssues = sprint.Issues.Count;

            return result;
        }

        public GetIssuesForSprintDto GetIssuesForSprint(GetPagedIssuesDto input)
        {
            var issues = _issueRepository.GetAllIncluding(x=>x.AssignedUser,x=>x.IssueCategory).Where(x=>x.SprintForeignKey == input.Id).ToList().Skip(input.ItemsPerPage * (input.PageNumber - 1)).Take(input.ItemsPerPage).ToList();
            var result = issues.MapTo<List<GetIssueInfoDto>>();
            var count = issues.Count;
            var resultDto = new GetIssuesForSprintDto
            {
                Issues = result,
                NrOfPages = (count + input.ItemsPerPage - 1) / input.ItemsPerPage
            };
            return resultDto;
        }

        public async Task<long> CreateOrUpdateIssue(CreateorUpdateIssueDto input)
        {
            if (input.Id.HasValue)
            {
                return await UpdateIssue(input);
            }
            else
            {
                return await CreateIssue(input);

            }
        }

        private async Task<long> CreateIssue(CreateorUpdateIssueDto input)
        {
            var issue = input.MapTo<Issue>();
            issue.IssueCategory = await _issueCategoryRepository.FirstOrDefaultAsync(x => x.Id == input.IssueCategoryForeignKey);
            issue.Sprint = await _sprintRepository.FirstOrDefaultAsync(x => x.Id == input.SprintForeignKey);
            issue.AssignedUser = await _userRepository.FirstOrDefaultAsync(x => x.Id == input.UserForeignKey);
            if (input.ParentIssueId.HasValue)
            {
                var parentIssue = _issueRepository.GetAllIncluding(x => x.SubIssues).FirstOrDefault(x => x.Id == input.ParentIssueId.Value);
                if (parentIssue.SubIssues == null)
                {
                    parentIssue.SubIssues = new List<Issue>();
                    parentIssue.SubIssues.Add(issue);
                }
                else
                {
                    parentIssue.SubIssues.Add(issue);
                }
               return await _issueRepository.InsertOrUpdateAndGetIdAsync(parentIssue);
            }
            return await _issueRepository.InsertAndGetIdAsync(issue);
        }

        private async Task<long> UpdateIssue(CreateorUpdateIssueDto input)
        {
            var issue = await _issueRepository.FirstOrDefaultAsync(x => x.Id == input.Id);

            var resultIssue = input.MapTo(issue);

            return await _issueRepository.InsertOrUpdateAndGetIdAsync(resultIssue);
        }

        public CreateorUpdateIssueDto GetIssueForEdit(IdInput<long> input)
        {
            var issue = _issueRepository.GetAllIncluding(x => x.AssignedUser, x => x.IssueCategory).FirstOrDefault(x => x.Id == input.Id);
            return issue.MapTo<CreateorUpdateIssueDto>();
        }

        public async Task DeleteIssue(IdInput<long> input)
        {
            var issue = await _issueRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
            if (issue.SubIssues.Any())
            {
                issue.SubIssues.Clear();
            }
            await _issueRepository.UpdateAsync(issue);
            await _issueRepository.DeleteAsync(input.Id);
        }

        public async Task<GetSprintDto> GetSprintForEdit(IdInput<long> input)
        {
            var sprint = await _sprintRepository.FirstOrDefaultAsync(x => x.Id == input.Id);

            return sprint.MapTo<GetSprintDto>();
        }

        public async Task LogTime(LogTimeDto input)
        {
            var issue = _issueRepository.GetAllIncluding(x => x.LoggedTime).FirstOrDefault(x => x.Id == input.IssueId);
            if (issue.LoggedTime == null)
            {
                issue.LoggedTime = new List<LoggedTime>();
            }
            var timeLog = input.MapTo<LoggedTime>();
            timeLog.User = _userRepository.FirstOrDefault(x => x.Id == AbpSession.UserId.Value);
            issue.LoggedTime.Add(timeLog);
            await _issueRepository.UpdateAsync(issue);
        }
    }
}
