using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using JJ.ProjectManagement.EntityFramework;

namespace JJ.ProjectManagement.Migrator
{
    [DependsOn(typeof(ProjectManagementDataModule))]
    public class ProjectManagementMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<ProjectManagementDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}