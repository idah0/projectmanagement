﻿(function () {
    angular.module('app').controller('app.views.issues.logTimeModal', [
        '$scope', '$modalInstance', 'issueId', 'abp.services.app.sprints',
        function ($scope, $modalInstance, issueId, sprintsAppService) {
           
            var vm = this;

            vm.logTime = {};
            vm.issueId = issueId;

            vm.save = function () {
                vm.logTime.issueId = vm.issueId;
                sprintsAppService.logTime(vm.logTime).success(function(result) {
                    $modalInstance.close();
                });
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            }

        }
    ]);
})();