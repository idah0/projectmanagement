﻿(function () {
    var controllerId = 'app.views.issues';
    angular.module('app').controller(controllerId, [
        '$scope', '$stateParams', '$state', 'abp.services.app.sprints', '$modal',
        function ($scope, $stateParams, $state, sprintsAppService, $modal) {
            var vm = this;

            vm.sprintId = parseInt($stateParams.sprintId);
            vm.projectId = parseInt($stateParams.projectId);

            vm.sprint = {};
            vm.issues = [];
            vm.nrOfPages = 1;

            vm.page = 1;

            vm.goBack = function() {
                $state.go('sprints', { projectId: vm.projectId });
            }

            vm.newIssue = function() {
                openModal(null, null, vm.projectId);
            }

            vm.newChildIssue = function(id) {
                openModal(null, id, vm.projectId);
            }

            vm.editIssue = function(id)
            {
                openModal(id, null, vm.projectId);
            }


            vm.deleteIssue = function (id) {
                abp.message.confirm(App.localize("AreYouSureDeleteIssue"),
                    function(confirm) {
                        if (confirm) {
                            sprintsAppService.deleteIssue({
                                id: id
                            }).success(function(result) {
                                vm.getIssues(1);
                            });
                        }
                    });

            }

            vm.logTime = function (id) {
                logTimeModal(id);
            }

            function logTimeModal(issueId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/issues/logTimeModal.cshtml',
                    controller: 'app.views.issues.logTimeModal as vm',
                    backdrop: 'static',
                    resolve: {
                        issueId: function () {
                            return issueId;
                        }
                    }
                });
            };

            function openModal(issueId, parentIssueId, projectId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/sprints/issueModal.cshtml',
                    controller: 'app.views.sprints.issueModal as vm',
                    backdrop: 'static',
                    resolve: {
                        issueId: function () {
                            return issueId;
                        },
                        projectId: function () {
                            return projectId;
                        },
                        sprintId: function() {
                            return vm.sprintId;
                        },
                        parentIssueId: function () {
                            return parentIssueId;
                        }
                    }
                });

                modalInstance.result.then(function (id) {
                    init();
                });
            };


            vm.showIssue = function(id) {
                $state.go('issue', {
                    projectId: vm.projectId,
                    issueId: id,
                    sprintId: vm.sprintId
                });
            };

            vm.changePage = function(val) {
                if (vm.page + val > vm.nrOfPages || vm.page + val <= 0) {
                    return;
                }
                vm.page = vm.page + val;
                vm.getIssues(vm.page);
            }

            vm.getIssues = function(page) {
                sprintsAppService.getIssuesForSprint({
                    id: vm.sprintId,
                    itemsPerPage: 5,
                    pageNumber: page
                }).success(function(result) {
                    vm.issues = result.issues;
                    vm.nrOfPages = result.nrOfPages;

                    $.each(vm.issues,
                        function (index, element) {
                            element.startDate = new Date(element.startDate);
                            element.endDate = new Date(element.endDate);

                        });
                });
            }

            function init() {
                sprintsAppService.getSprintInfo({ id: vm.sprintId }).success(function(result) {
                    vm.sprint = result;
                });

                vm.getIssues(1);

            }

            init();
        }
    ]);
})();