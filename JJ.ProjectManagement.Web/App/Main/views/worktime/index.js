﻿(function () {
    var controllerId = 'app.views.workTime';
    angular.module('app').controller(controllerId, [
        '$scope', 'abp.services.app.workTime',
        function ($scope, workTimeAppService) {
            var vm = this;

            var thisDate = new Date();
            vm.year = thisDate.getYear()-100 + 2000;
            vm.month = thisDate.getMonth()+1;
            vm.loggedTime = [];

            vm.months =
                [
                {
                    id: 1,
                    name: "01"
                },
                {
                    id: 2,
                    name: "02"
                },
                {
                    id: 3,
                    name: "03"
                },
                {
                    id: 4,
                    name: "04"
                },
                {
                    id: 5,
                    name: "05"
                },
                {
                    id: 6,
                    name: "06"
                },
                {
                    id: 7,
                    name: "07"
                },
                {
                    id: 8,
                    name: "08"
                },
                {
                    id: 9,
                    name: "09"
                },
                {
                    id: 10,
                    name: "10"
                },
                {
                    id: 11,
                    name: "11"
                },
                {
                    id: 12,
                    name: "12"
                }
                ];
            vm.years = [];

            function generateYears() {
                for (var i = -2; i < 3; i++) {
                    vm.years.push({
                        id: vm.year + i,
                        name: vm.year + i + ''
                    });
                }
            }

            vm.getWorkTime = function () {
                workTimeAppService.getWorkTime(
                {
                    year: vm.year,
                    month: vm.month
                }).success(function(result) {
                    vm.loggedTime = result;


                });
            }

            function init() {
                generateYears();
                vm.getWorkTime();
            }

            init();
        }
    ]);
})();