﻿(function () {
    var controllerId = 'app.views.sprints';
    angular.module('app').controller(controllerId,
    [
        '$scope', '$stateParams', 'abp.services.app.projects', '$state', '$modal',
        function($scope, $stateParams, projectsAppService, $state, $modal) {
            var vm = this;

            vm.project = {};
            vm.projectId = parseInt($stateParams.projectId);

            vm.goToSprint = function(id) {
                $state.go('issues', { sprintId: id, projectId: vm.projectId });
            }

            vm.goBack = function() {
                $state.go('projects');
            }

            vm.createSprint = function () {
                openModal(vm.projectId,null);
            }

            vm.editSprint = function(id) {
                openModal(vm.projectId, id);
            }
            vm.deleteSprint = function(id) {
                abp.message.confirm(App.localize("DeleteSprintInfo"),
                    function(confirm) {
                        if (confirm) {
                            projectsAppService.deleteSprint({
                                id: id
                            }).success(function(result) {
                                init();
                            });
                        }
                    });
            }

            vm.editMembers = function() {
                openEditMembersModal(vm.projectId);
            }

            function openEditMembersModal(projectId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/sprints/editMembers.cshtml',
                    controller: 'app.views.sprints.editMembers as vm',
                    backdrop: 'static',
                    resolve: {
                        projectId: function () {
                            return projectId;
                        }
                    }
                });

                modalInstance.result.then(function (id) {
                    init();
                });
            };



            function openModal(projectId, sprintId) {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/sprints/createModal.cshtml',
                    controller: 'app.views.sprints.createModal as vm',
                    backdrop: 'static',
                    resolve: {
                        projectId: function() {
                            return projectId;
                        },
                        sprintId: function () {
                            return sprintId;
                        }
                    }
                });

                modalInstance.result.then(function (id) {
                    init();
                });
            };

            function init() {
                projectsAppService.getSingleProject({
                    id: vm.projectId
                }).success(function(result) {
                    vm.project = result;

                    $.each(vm.project.sprints,
                        function(index, element) {
                            element.startDate = new Date(element.startDate);
                            element.endDate = new Date(element.endDate);
                        });
                });
            }


            init();
        }
    ]);
})();