﻿(function () {
    angular.module('app').controller('app.views.sprints.editMembers', [
        '$scope', '$modalInstance', 'abp.services.app.projects', 'projectId', 'abp.services.app.dictionary',
        function ($scope, $modalInstance, projectAppService, projectId, dictionaryAppService) {
            var vm = this;

            vm.members = [];

            vm.users = [];

            vm.projectId = projectId;

            vm.addMember = function () {
                vm.members.push({
                    membersCategoryId: 0,
                    userId: 0
                });
            }

            vm.memberCategories = [];

            vm.save = function () {
                projectAppService.addMembersToProject({
                    projectId: vm.projectId,
                    members: vm.members
                }).success(function (result) {
                    $modalInstance.close();
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            function getDicitionaries() {
                dictionaryAppService.getAllUsers().success(function (result) {
                    vm.users = result;
                });
                dictionaryAppService.getMembersCategories().success(function (result) {
                    vm.memberCategories = result;
                });

                dictionaryAppService.getAssignedUsers({
                    id: vm.projectId
                }).success(function (result) {
                    $.each(result, function (index, element) {
                        vm.members.push({
                            userId: element.user.id,
                            membersCategoryId: element.memberCategory.id
                        });
                    });
                    vm.addMember();

                });
            }

            function init() {
                getDicitionaries();
            }

            init();
        }
    ]);
})();