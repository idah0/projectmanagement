﻿(function () {
    angular.module('app').controller('app.views.sprints.createModal', [
        '$scope', '$modalInstance', 'abp.services.app.projects', 'projectId', 'sprintId', 'abp.services.app.sprints',
        function ($scope, $modalInstance, projectAppService, projectId, sprintId, sprintsAppService) {
            var vm = this;

            vm.sprint = {};

            vm.sprintId = sprintId;
            vm.projectId = projectId;

            vm.save = function () {
                vm.sprint.id = vm.sprintId;
                vm.sprint.projectId = vm.projectId;

                projectAppService.createOrUpdateSprint(vm.sprint).success(function (result) {
                    $modalInstance.close(result);
                });
            };

            vm.cancel = function () {             
                $modalInstance.dismiss();
            };

            function init() {
                if (vm.sprintId !== null) {
                    sprintsAppService.getSprintForEdit({
                        id: vm.sprintId
                    }).success(function (result) {
                        vm.sprint = result;
                        vm.sprint.startDate = new Date(result.startDate);
                        vm.sprint.endDate = new Date(result.endDate);
                    });
                }
            }

            init();
        }
    ]);
})();