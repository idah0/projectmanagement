﻿(function () {
    angular.module('app').controller('app.views.sprints.issueModal', [
        '$scope', '$modalInstance', 'abp.services.app.sprints', 'abp.services.app.dictionary', 'sprintId', 'issueId', 'parentIssueId','projectId',
        function ($scope, $modalInstance, sprintsAppService, dictionaryAppService, sprintId, issueId, parentIssueId, projectId) {
            var vm = this;

            vm.issue = {};


            vm.sprintId = sprintId;
            vm.projectId = projectId;
            vm.issueId = issueId;
            vm.parentIssueId = parentIssueId;
            vm.issueCategories = [];
            vm.members = [];

            vm.doneSelect = [
                {
                    id: 0,
                    value: "0%"
                },
                {
                    id: 10,
                    value: "10%"
                },
                {
                    id: 20,
                    value: "20%"
                },
                {
                    id: 30,
                    value: "30%"
                },
                {
                    id: 40,
                    value: "40%"
                },
                {
                    id: 50,
                    value: "50%"
                },
                {
                    id: 60,
                    value: "60%"
                },
                {
                    id: 70,
                    value: "70%"
                },
                {
                    id: 80,
                    value: "80%"
                },
                {
                    id: 90,
                    value: "90%"
                },
                {
                    id: 100,
                    value: "100%"
                }
            ];

            vm.save = function () {
                vm.issue.sprintForeignKey = vm.sprintId;
                vm.issue.parentIssueId = vm.parentIssueId;
                sprintsAppService.createOrUpdateIssue(vm.issue).success(function (result) {
                    $modalInstance.close();
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            function getDictionaries() {
                dictionaryAppService.getIssueCategories().success(function (result) {
                    vm.issueCategories = result;
                });
                dictionaryAppService.getAssignedUsers({
                    id: vm.projectId
                }).success(function (result) {
                    vm.members = result;
                });
            }


            function init() {
                getDictionaries();
                if (vm.issueId !== null) {
                    sprintsAppService.getIssueForEdit({
                        id: vm.issueId
                    }).success(function (result) {
                        vm.issue = result;
                        vm.issue.startDate = new Date(result.startDate);
                        vm.issue.endDate = new Date(result.endDate);

                    });
                }
            }

            init();
        }
    ]);
})();