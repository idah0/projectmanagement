﻿(function () {
    angular.module('app').controller('app.views.projects.createModal', [
        '$scope', '$modalInstance', 'abp.services.app.projects',
        function ($scope, $modalInstance, projectAppService) {
            var vm = this;

            vm.name = "";

            vm.save = function () {
                projectAppService.createOrUpdateProject({
                    id: null,
                    Name: vm.name
                }).success(function (result) {
                    $modalInstance.close(result);
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();