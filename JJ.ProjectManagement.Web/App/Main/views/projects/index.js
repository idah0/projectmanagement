﻿(function () {
    var controllerId = 'app.views.projects';
    angular.module('app').controller(controllerId, [
        '$scope', 'abp.services.app.projects', '$state', '$modal',
        function ($scope, projectsAppService, $state, $modal) {
            var vm = this;

            vm.projects = [];

            vm.goToProject = function (id) {
                $state.go('sprints', { projectId: id });
            }


            vm.createProject = function () {
                openModal();
            }
            function openModal() {
                var modalInstance = $modal.open({
                    templateUrl: '/App/Main/views/projects/createModal.cshtml',
                    controller: 'app.views.projects.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (id) {
                    vm.goToProject(id);
                });
            };


            function init() {
                projectsAppService.getAllProjects().success(function (result) {
                    vm.projects = result;
                });
            }

            init();
        }
    ]);
})();