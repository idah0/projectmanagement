﻿(function () {
    var controllerId = 'app.views.singleIssue';
    angular.module('app').controller(controllerId,
        [
            '$scope', 'abp.services.app.sprints', 'abp.services.app.dictionary', '$stateParams','$state',
            function ($scope, sprintsAppService, dictionaryAppService, $stateParams,$state) {
                var vm = this;

                vm.issueCategories = [];
                vm.issue = {};
                vm.members = [];

                vm.sprintId = parseInt($stateParams.sprintId);
                vm.projectId = parseInt($stateParams.projectId);
                vm.issueId = parseInt($stateParams.issueId);

                function getDictionaries() {
                    dictionaryAppService.getIssueCategories().success(function (result) {
                        vm.issueCategories = result;
                    });
                    dictionaryAppService.getAssignedUsers({
                        id: vm.projectId
                    }).success(function (result) {
                        vm.members = result;
                        $.each(vm.members,
                            function(index, element) {
                                if (element.user.id === vm.issue.userForeignKey) {
                                    vm.issue.member = element.user;
                                }
                            });
                    });
                }

                vm.goBack = function() {
                    $state.go('issues',
                    {
                        sprintId: vm.sprintId,
                        projectId: vm.projectId
                    });
                }

                function init() {
                    sprintsAppService.getIssueForEdit({
                        id: vm.issueId
                    }).success(function (result) {
                        vm.issue = result;
                        vm.issue.startDate = new Date(result.startDate);
                        vm.issue.endDate = new Date(result.endDate);
                        getDictionaries();
                    });

                }

                init();
            }
        ]);
})();