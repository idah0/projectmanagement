﻿(function () {
    'use strict';
    
    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',

        'abp'
    ]);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');

            if (abp.auth.hasPermission('Pages.Users')) {
                $stateProvider
                    .state('users', {
                        url: '/users',
                        templateUrl: '/App/Main/views/users/index.cshtml',
                        menu: 'Users' //Matches to name of 'Users' menu in ProjectManagementNavigationProvider
                    });
               // $urlRouterProvider.otherwise('/users');
            }


            $stateProvider
                .state('home',
                {
                    url: '/',
                    templateUrl: '/App/Main/views/home/home.cshtml',
                    menu: 'Home' //Matches to name of 'Home' menu in ProjectManagementNavigationProvider
                });

            $stateProvider.state('projects',
            {
                url: '/projects',
                templateUrl: '/App/Main/views/projects/index.cshtml',
                menu: 'Projects'
                });
            $urlRouterProvider.otherwise('/projects');

            $stateProvider.state('sprints',
                {
                    url: '/sprints/:projectId',
                    templateUrl: '/App/Main/views/sprints/index.cshtml',
                });
            $stateProvider.state('issues',
                {
                    url: '/issues/:projectId/:sprintId',
                    templateUrl: '/App/Main/views/issues/index.cshtml'
                    
                });
            $stateProvider.state('issue',
                {
                    url: '/issue/:projectId/:sprintId/:issueId',
                    templateUrl: '/App/Main/views/singleIssue/index.cshtml'

                });
            $stateProvider.state('worktime',
                {
                    url: '/worktime',
                    templateUrl: '/App/Main/views/workTime/index.cshtml'

                });
        }
    ]);
})();