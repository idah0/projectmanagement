﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace JJ.ProjectManagement.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : ProjectManagementControllerBase
    {
        public ActionResult Index()
        {
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }
	}
}