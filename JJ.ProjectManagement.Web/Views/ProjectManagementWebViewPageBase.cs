﻿using Abp.Web.Mvc.Views;

namespace JJ.ProjectManagement.Web.Views
{
    public abstract class ProjectManagementWebViewPageBase : ProjectManagementWebViewPageBase<dynamic>
    {

    }

    public abstract class ProjectManagementWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected ProjectManagementWebViewPageBase()
        {
            LocalizationSourceName = ProjectManagementConsts.LocalizationSourceName;
        }
    }
}