﻿using Abp.Application.Navigation;
using Abp.Localization;
using JJ.ProjectManagement.Authorization;

namespace JJ.ProjectManagement.Web
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class ProjectManagementNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                //.AddItem(
                //    new MenuItemDefinition(
                //        "Home",
                //        new LocalizableString("HomePage", ProjectManagementConsts.LocalizationSourceName),
                //        url: "#/",
                //        icon: "fa fa-home"
                //    )
                //).AddItem(
                //    new MenuItemDefinition(
                //        "Tenants",
                //        L("Tenants"),
                //        url: "#tenants",
                //        icon: "fa fa-globe",
                //        requiredPermissionName: PermissionNames.Pages_Tenants
                //    )
                //)
                .AddItem(
                    new MenuItemDefinition(
                        "Users",
                        L("Users"),
                        url: "#users",
                        icon: "fa fa-users",
                        requiredPermissionName: PermissionNames.Pages_Users
                    )
                ).AddItem(new MenuItemDefinition(
                    "Projects",
                    L("Projects"),
                    url: "#projects",
                    icon: "fa fa-cog")
                    )
                 .AddItem(new MenuItemDefinition(
                    "WorkTime",
                    L("WorkTime"),
                    url: "#worktime",
                    icon: "fa fa-clock-o"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ProjectManagementConsts.LocalizationSourceName);
        }
    }
}
